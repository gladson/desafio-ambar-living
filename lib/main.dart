import 'package:flutter/material.dart';

import 'package:desafio_ambar_living/themes/v1_theme.dart';
import 'package:desafio_ambar_living/pages/splashscreen/v2_splashscreen.dart';
import 'package:desafio_ambar_living/pages/github_list_page.dart';

void main() {
  runApp(DesafioApp());
}

class DesafioApp extends StatelessWidget {
  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Desafio - Ambar Living',
      theme: v1Theme,
      debugShowCheckedModeBanner: false,
      home: V2SplashScreen(
        loadTimeSec: 3,
        loading: true,
        nextScreen: GithubListPage(),
        assetOrNetworkImage: true,
        assetImage: './assets/images/logo_ambar_living.webp',
        otherPageContext: context,
      ),
    );
  }
}
