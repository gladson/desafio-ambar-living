import 'package:flutter/material.dart';

import 'package:dio/dio.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:desafio_ambar_living/components/animated_fade_in.dart';
import 'package:desafio_ambar_living/pages/splashscreen/v2_splashscreen.dart';
import 'package:desafio_ambar_living/models/public_repositories_model.dart';
import 'package:desafio_ambar_living/repositories/public_repositories_repository.dart';

class GithubListPage extends StatefulWidget {
  GithubListPage({Key key}) : super(key: key);

  @override
  _GithubListPageState createState() => _GithubListPageState();
}

class _GithubListPageState extends State<GithubListPage> {
  final _repository = PublicRepositoriesRepository(Dio());

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        // headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Não foi possível abrir - $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<PublicRepositoriesModel>>(
        future: _repository.findAll(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return V2SplashScreen(
              loadTimeSec: 5,
              loading: false,
              nextScreen: GithubListPage(),
              assetOrNetworkImage: true,
              assetImage: './assets/images/logo_ambar_living.webp',
              otherPageContext: context,
            );
            // return Center(
            //   child: CircularProgressIndicator(),
            // );
          }
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            final repositories = snapshot.data;
            return AnimatedFadeIn(
              delay: 1.5,
              child: ListView.builder(
                itemCount: repositories.length,
                itemBuilder: (context, index) {
                  final repositoryUser = repositories[index];
                  return ListTile(
                    title: Text(repositoryUser.fullName == null
                        ? ""
                        : repositoryUser.fullName),
                    subtitle: Text(repositoryUser.description == null
                        ? ""
                        : repositoryUser.description),
                    leading: Image(
                      image: NetworkImage(repositoryUser.owner.avatarUrl),
                      width: 50,
                    ),
                    onTap: () {
                      _launchInBrowser(repositoryUser.htmlUrl);
                    },
                  );
                },
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
